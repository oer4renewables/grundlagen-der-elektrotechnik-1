EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5DEE6D75
P 1500 4050
F 0 "R1" H 1570 4096 50  0000 L CNN
F 1 "50" H 1570 4005 50  0000 L CNN
F 2 "Resistors_THT:R_Array_SIP4" V 1430 4050 50  0001 C CNN
F 3 "~" H 1500 4050 50  0001 C CNN
	1    1500 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5DEE7930
P 2100 3900
F 0 "R2" H 2170 3946 50  0000 L CNN
F 1 "5" H 2170 3855 50  0000 L CNN
F 2 "Resistors_THT:R_Array_SIP4" H 2170 3809 50  0001 L CNN
F 3 "~" H 2100 3900 50  0001 C CNN
	1    2100 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5DEE7F48
P 2800 3700
F 0 "R3" H 2870 3746 50  0000 L CNN
F 1 "5m" H 2870 3655 50  0000 L CNN
F 2 "Resistors_THT:R_Array_SIP4" V 2730 3700 50  0001 C CNN
F 3 "~" H 2800 3700 50  0001 C CNN
	1    2800 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5DEE85A4
P 3400 3500
F 0 "R4" H 3470 3546 50  0000 L CNN
F 1 "5k" H 3470 3455 50  0000 L CNN
F 2 "Resistors_THT:R_Array_SIP4" V 3330 3500 50  0001 C CNN
F 3 "~" H 3400 3500 50  0001 C CNN
	1    3400 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5DEE8B1A
P 4100 2950
F 0 "R5" H 4170 2996 50  0000 L CNN
F 1 "500u" H 4170 2905 50  0000 L CNN
F 2 "Resistors_THT:R_Array_SIP4" V 4030 2950 50  0001 C CNN
F 3 "~" H 4100 2950 50  0001 C CNN
	1    4100 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5DEE8C0F
P 4550 3050
F 0 "R6" H 4620 3096 50  0000 L CNN
F 1 "500" H 4620 3005 50  0000 L CNN
F 2 "Resistors_THT:R_Array_SIP4" V 4480 3050 50  0001 C CNN
F 3 "~" H 4550 3050 50  0001 C CNN
	1    4550 3050
	1    0    0    -1  
$EndComp
$Comp
L pspice:VSOURCE V1
U 1 1 5DEE8E10
P 4100 4000
F 0 "V1" H 4328 4046 50  0000 L CNN
F 1 "5" H 4328 3955 50  0000 L CNN
F 2 "Housings_DFN_QFN:AMS_QFN-4-1EP_2x2mm_Pitch0.95mm" H 4100 4000 50  0001 C CNN
F 3 "~" H 4100 4000 50  0001 C CNN
	1    4100 4000
	1    0    0    -1  
$EndComp
$Comp
L pspice:ISOURCE I1
U 1 1 5DEE9CD4
P 1800 2950
F 0 "I1" H 2030 2996 50  0000 L CNN
F 1 "5" H 2030 2905 50  0000 L CNN
F 2 "Battery_Holders:Keystone_103_1x20mm-CoinCell" H 1800 2950 50  0001 C CNN
F 3 "~" H 1800 2950 50  0001 C CNN
	1    1800 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5DEEA254
P 4100 4350
F 0 "#PWR01" H 4100 4100 50  0001 C CNN
F 1 "GND" H 4105 4177 50  0000 C CNN
F 2 "" H 4100 4350 50  0001 C CNN
F 3 "" H 4100 4350 50  0001 C CNN
	1    4100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4300 4100 4350
Wire Wire Line
	1500 4200 1800 4200
Wire Wire Line
	1800 4200 1800 3350
Wire Wire Line
	1800 4200 2100 4200
Wire Wire Line
	2100 4200 2100 4050
Connection ~ 1800 4200
Wire Wire Line
	2100 4200 2100 4300
Wire Wire Line
	2100 4300 4100 4300
Connection ~ 2100 4200
Connection ~ 4100 4300
Wire Wire Line
	2100 3750 2100 3350
Wire Wire Line
	2100 3350 3400 3350
Wire Wire Line
	2800 3550 1500 3550
Wire Wire Line
	1500 3550 1500 3900
Wire Wire Line
	2800 3850 3400 3850
Wire Wire Line
	3400 3850 3400 3650
Wire Wire Line
	3400 3350 3700 3350
Wire Wire Line
	4550 3350 4550 3200
Connection ~ 3400 3350
Wire Wire Line
	4300 3350 4550 3350
Wire Wire Line
	3700 3350 3700 3700
Wire Wire Line
	3700 3700 4100 3700
Wire Wire Line
	1800 2550 3550 2550
Wire Wire Line
	4550 2550 4550 2900
Wire Wire Line
	3550 3150 3550 2550
Connection ~ 3550 2550
Wire Wire Line
	3550 2550 4550 2550
Wire Wire Line
	4100 2800 4300 2800
Wire Wire Line
	4300 2800 4300 3350
Connection ~ 4300 3350
Wire Wire Line
	4300 3350 4300 3450
Wire Wire Line
	4300 3450 4100 3450
Wire Wire Line
	4100 3450 4100 3700
Connection ~ 4100 3700
Wire Wire Line
	3550 3150 4100 3150
Wire Wire Line
	4100 3150 4100 3100
$EndSCHEMATC
