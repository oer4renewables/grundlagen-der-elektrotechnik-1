EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R 1k1
U 1 1 5DEE4369
P 1200 1300
F 0 "1k1" V 1280 1300 50  0000 C CNN
F 1 "R" V 1200 1300 50  0000 C CNN
F 2 "" V 1130 1300 50  0001 C CNN
F 3 "" H 1200 1300 50  0001 C CNN
	1    1200 1300
	1    0    0    -1  
$EndComp
$Comp
L R 5k1
U 1 1 5DEE43B8
P 1550 1300
F 0 "5k1" V 1630 1300 50  0000 C CNN
F 1 "R" V 1550 1300 50  0000 C CNN
F 2 "" V 1480 1300 50  0001 C CNN
F 3 "" H 1550 1300 50  0001 C CNN
	1    1550 1300
	1    0    0    -1  
$EndComp
$Comp
L R 1k2
U 1 1 5DEE4417
P 1850 1300
F 0 "1k2" V 1930 1300 50  0000 C CNN
F 1 "R" V 1850 1300 50  0000 C CNN
F 2 "" V 1780 1300 50  0001 C CNN
F 3 "" H 1850 1300 50  0001 C CNN
	1    1850 1300
	1    0    0    -1  
$EndComp
$Comp
L R 1.2k1
U 1 1 5DEE447A
P 2250 1300
F 0 "1.2k1" V 2330 1300 50  0000 C CNN
F 1 "R" V 2250 1300 50  0000 C CNN
F 2 "" V 2180 1300 50  0001 C CNN
F 3 "" H 2250 1300 50  0001 C CNN
	1    2250 1300
	1    0    0    -1  
$EndComp
$Comp
L Battery_Cell 3.2
U 1 1 5DEE44DD
P 1200 1650
F 0 "3.2" H 1300 1750 50  0000 L CNN
F 1 "Battery_Cell" H 1300 1650 50  0000 L CNN
F 2 "" V 1200 1710 50  0001 C CNN
F 3 "" V 1200 1710 50  0001 C CNN
	1    1200 1650
	1    0    0    -1  
$EndComp
$Comp
L Battery_Cell 3.4
U 1 1 5DEE4554
P 1550 1650
F 0 "3.4" H 1650 1750 50  0000 L CNN
F 1 "Battery_Cell" H 1650 1650 50  0000 L CNN
F 2 "" V 1550 1710 50  0001 C CNN
F 3 "" V 1550 1710 50  0001 C CNN
	1    1550 1650
	1    0    0    -1  
$EndComp
$Comp
L Battery_Cell 3.3
U 1 1 5DEE45BF
P 1850 1650
F 0 "3.3" H 1950 1750 50  0000 L CNN
F 1 "Battery_Cell" H 1950 1650 50  0000 L CNN
F 2 "" V 1850 1710 50  0001 C CNN
F 3 "" V 1850 1710 50  0001 C CNN
	1    1850 1650
	1    0    0    -1  
$EndComp
$Comp
L Battery_Cell 3.35
U 1 1 5DEE460E
P 2250 1650
F 0 "3.35" H 2350 1750 50  0000 L CNN
F 1 "Battery_Cell" H 2350 1650 50  0000 L CNN
F 2 "" V 2250 1710 50  0001 C CNN
F 3 "" V 2250 1710 50  0001 C CNN
	1    2250 1650
	1    0    0    -1  
$EndComp
$Comp
L D_Photo D1
U 1 1 5DEE4720
P 3100 1450
F 0 "D1" H 3120 1520 50  0000 L CNN
F 1 "D_Photo" H 3060 1340 50  0000 C CNN
F 2 "" H 3050 1450 50  0001 C CNN
F 3 "" H 3050 1450 50  0001 C CNN
	1    3100 1450
	0    1    1    0   
$EndComp
$Comp
L D_Photo D2
U 1 1 5DEE47BD
P 3450 1450
F 0 "D2" H 3470 1520 50  0000 L CNN
F 1 "D_Photo" H 3410 1340 50  0000 C CNN
F 2 "" H 3400 1450 50  0001 C CNN
F 3 "" H 3400 1450 50  0001 C CNN
	1    3450 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	2250 1250 2250 1150
Wire Wire Line
	1200 1150 3450 1150
Connection ~ 1550 1150
Wire Wire Line
	1850 1150 1850 1150
Connection ~ 1850 1150
Wire Wire Line
	1200 1750 3450 1750
Connection ~ 1550 1750
Connection ~ 1850 1750
Wire Wire Line
	3100 1750 3100 1550
Connection ~ 2250 1750
Wire Wire Line
	3450 1750 3450 1550
Connection ~ 3100 1750
Wire Wire Line
	3100 1150 3100 1250
Connection ~ 2250 1150
Wire Wire Line
	3450 1150 3450 1250
Connection ~ 3100 1150
$Comp
L GND #PWR01
U 1 1 5DEE4E54
P 3100 1750
F 0 "#PWR01" H 3100 1500 50  0001 C CNN
F 1 "GND" H 3100 1600 50  0000 C CNN
F 2 "" H 3100 1750 50  0001 C CNN
F 3 "" H 3100 1750 50  0001 C CNN
	1    3100 1750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
